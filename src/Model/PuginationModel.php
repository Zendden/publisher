<?php

namespace App\Model;

use App\Repository\PublicRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Publicated;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PuginationModel extends ServiceEntityRepository
{
    /**
     * Amount of the 'publicateds'.
     * @var int
     */
    protected $rowsPugination = 5;

    /**
     * Number of current page
     * @var int
     */
    protected $currentPage;

    /**
     * Pugination buttons
     * @return array
     */
    protected $puginationButtons = [];


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Publicated::class);
    }


    /**
     * @param int $id
     * @param PuginationModel $puginationModel
     * @return array
     */
    public function showListByPugination ($id, PuginationModel $puginationModel)
    {
        $oneListByPublicated = $puginationModel->findByPugination($id);

        return $oneListByPublicated;
    }

    /**
     * @param PuginationModel $puginationModel
     * @return []
     */
    public function showPugination (PuginationModel $puginationModel)
    {
        $countPages = $puginationModel->countPublicated ();

        $counter = $countPages[0][1];
        $counter = (int) $counter;
        $pageCounter = ($counter/5);

        for ($i=0; $i < $pageCounter; $i++){
            $this->puginationButtons[$i] = [$i];
        }

        // return $pageCounter;
        return $this->puginationButtons;
    }

    /**
     * @return $publicated[]
     * @param int
     */
    public function findByPugination ($currentPage)
    {
        $this->currentPage = $currentPage; #актуальная страница
        $this->rowsCounter = 5; #всего записей

        return $this->createQueryBuilder('p')
            ->select ('p')
            // ->where('p.id > :val')
            // ->setParameter('val', $this->currentPage * 5)
            ->setFirstResult($this->rowsCounter * ($this->currentPage))
            ->setMaxResults($this->rowsCounter)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return int[]
     */
    public function countPublicated ()
    {
        return $this->createQueryBuilder ('publicated')
            ->select ('count(publicated)')
            ->andWhere ('publicated.id > :val')
            ->setParameter ('val', -1)
            ->getQuery ()
            ->getResult ();
    }
}