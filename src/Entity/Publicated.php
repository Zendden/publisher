<?php 

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PublicRepository")
 */
class Publicated
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @var integer
     */
    private $id;

    /**
     * @var string $title
     * @ORM\Column(type="string", length=150)
     */
    protected $title;
    
    /**
     * @var string $body
     * @ORM\Column(type="string", length=250)
     */
    protected $body;


    /**
     * Getter
     * @return string|null
     */
    public function getTitle (): ?string
    {
        return $this->title;
    }

    /**
     * Getter
     * @return string|null
     */
    public function getBody (): ?string
    {
        return $this->body;
    }

    /**
     * Getter
     * @return integer|null $id
     */
    public function getId (): ?int
    {
        return $this->id;
    }


    /**
     * Setter
     * @param string $title
     * @return void
     */
    public function setTitle ($title): void
    {
        $this->title = $title;
    }

    /**
     * Setter
     * @param string $body
     * @return void
     */
    public function setBody ($body): void 
    {
        $this->body = $body;
    }


}