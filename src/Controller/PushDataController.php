<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Mercure\Publisher;

class PushDataController extends AbstractController {

    /**
     * @Route("/push-data", name="push")
     * 
     * @param Publisher $publisher
     * @return Response
     */
    public function __invoke (Publisher $publisher): Response {

        $update = new Update (
            "http://localhost:8000/show-publicated/5/page",
            "[]"
        );

        $publisher ($update);

        return new Response (
            "Publisher! Success!"
        );
    }

    /**
     * @Route("/ping", name="ping", methods={"POST"})
     */
    public function ping (Publisher $publisher) {
        $update = new Update (
            "http://localhost:8000/show-concrete-publicated",
            "Some data!"
        );

        $publisher($update);

        return $this->redirectToRoute("show-concrete-publicated");
    }
}