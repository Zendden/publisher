<?php 
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Publicated;
use Doctrine\Common\Persistence\ObjectManager;
use App\Controller\PuginationController;
use App\Model\PuginationModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;

class PublisherController extends AbstractController
{
    /**
     * @Route("/add-publicated", name="add-publicated")
     */
    public function showForm (Request $request)
    {
        $newPost = new Publicated ();

        $form = $this->createFormBuilder ($newPost)
            ->add ("title")
            ->add ("body")
            ->add ("save", SubmitType::class, ["label" => "Send data"])
            ->getForm ();

        $form->handleRequest ($request);

        if ($form->isSubmitted () && $form->isValid ())
        {
            $newPost = $form->getData ();

            $em = $this->getDoctrine()->getManager();
            $em->persist ($newPost);
            $em->flush ();

            return $this->redirectToRoute("show-publicated");
        }

        return $this->render ("base.html.twig", 
        [
            "form" => $form->createView (),
            "content" => "",
        ]);
    }

    /**
     * @Route("/show-publicated/{currentPage}/page", name="show-publicated")
     */
    public function showPublicated ($currentPage=0, PuginationModel $puginationModel)
    {
        $puginationListWithPublicateds = $puginationModel->showListByPugination ($currentPage, $puginationModel);
        $puginationRowButtons = $puginationModel->showPugination ($puginationModel);

        return $this->render ("/Publicated/showAllPublicated.html.twig", [
            "publicateds" => $puginationListWithPublicateds,
            "buttons" => $puginationRowButtons,
        ]);
    }

    /**
     * @Route("/show-concrete-publicated/{id}", name="show-concrete-publicated")
     */
    public function showConcretePublicated ($id=1, ObjectManager $objectManager)
    {
        $publicated = $objectManager->getRepository (Publicated::class)->find ($id);

        return $this->render ("/Publicated/showPublicated.html.twig", [
            "publicated" => $publicated,
        ]);
    }

    /**
     * @Route("/publicated/{id}/edit", name="edit-publicated")
     */
    public function editConcretePublicated(Request $request, $id, Publicated $publicated, ObjectManager $obmanager)
    {
        $publicated = $obmanager->getRepository(Publicated::class)->find($id);

        $form = $this->createFormBuilder($publicated)
            ->add("title")
            ->add("body")
            ->add("save", SubmitType::class, ["label" => "Push edition"])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($publicated);
            $em->flush();

            return $this->redirectToRoute("show-publicated");
        }

         return $this->render("base.html.twig", [
             "form" => $form->createView(),
             "content" => ""
         ]);
    }

    /**
     * @Route("/publicated/{id}/delete", name="delete-publicated")
     * @return void
     */
    public function deleteConcretePublicated($id)
    {
        $em = $this->getDoctrine()->getManager();

        $publicated = $em->getRepository(Publicated::class)->find($id);

        $em->remove($publicated);
        $em->flush();

        return $this->redirectToRoute("show-publicated");
    }
}